//
//  WalkThrough3ViewController.swift
//  NinjaWay
//
//  Created by Apple on 14/03/19.
//  Copyright © 2020 bucketlist. All rights reserved.
//

import UIKit

class WalkThrough3ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnPrevAction() {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: WalkThrough2ViewController.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
