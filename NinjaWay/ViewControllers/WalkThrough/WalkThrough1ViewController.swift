//
//  WalkThrough1ViewController.swift
//  NinjaWay
//
//  Created by Apple on 05/03/19.
//  Copyright © 2020 bucketlist. All rights reserved.
//

import UIKit

class WalkThrough1ViewController: UIViewController {

    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }

    @IBAction func btnNextAction() {
        self.performSegue(withIdentifier: "toNext", sender: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
