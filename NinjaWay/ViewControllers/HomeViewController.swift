//
//  HomeViewController.swift
//  NinjaWay
//
//  Created by kdstudio on 22/11/18.
//  Copyright © 2020 bucketlist. All rights reserved.
//

import UIKit
import GoogleMobileAds
//import FBSDKLoginKit

let kLiveUnitID = "ca-app-pub-1129577948525988/5641497891"
let kTestUnitID = "ca-app-pub-3940256099942544/2934735716"

class HomeViewController: UIViewController, GADBannerViewDelegate {//FBSDKLoginButtonDelegate {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var blurView: UIView!
    @IBOutlet var widthMenu: NSLayoutConstraint!
    @IBOutlet var btnContactUs: UIButton!
    @IBOutlet weak var btnPopUpCancel: UIButton!
    @IBOutlet weak var bannerView: GADBannerView!

    var hamburgerMenuIsVisible = false
//    let facebookLogin = LoginManager()
//    let facebookPermissions = ["public_profile", "email", "user_friends"]
    
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        bannerView.adSize = kGADAdSizeSmartBannerPortrait
        bannerView.adUnitID =  kTestUnitID
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        bannerView.delegate = self
        
        // Do any additional setup after loading the view.
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        setUpView()
        
        //Did tap on Blur View
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTapofBlurView(sender:)))
        blurView.addGestureRecognizer(tap)
        
//        // create facebook login button programmatically
//        let loginButton = FBLoginButton()
//        loginButton.center = view.center
//
//        loginButton.permissions = facebookPermissions
////        loginButton.delegate = self
//        view.addSubview(loginButton)
//
//
//        if (AccessToken.current != nil)
//        {
//            // user logged in to facebook already
//        }
//        else {
//
//
//        }
    }
    
//    //MARK: FBSDKLoginButtonDelegate
//    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
//        if ((error) != nil) {
//            // Process error
//        }
//        else if result.isCancelled {
//            // Handle cancellations
//        }
//        else {
//            // Navigate to other view
//            print ("facebook login complete")
//        }
//    }
//
//    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
//                print ("logged out of facebook")
//    }
   

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Action Methods
    
    @objc func handleTapofBlurView(sender: UITapGestureRecognizer? = nil) {
        imgView.isHidden = true
        blurView.isHidden = true
    }
    
    @IBAction func btnPOpupCancelAction(_ sender: Any) {
        imgView.isHidden = true
        blurView.isHidden = true
        
    }
    @IBAction func btnMenuAction(_ sender: Any) {
     
        menuClicked()

    }
    
    @IBAction func btnLink1(_ sender: Any) {
        
        if let url = URL(string: "https://www.bucketlist.co.jp/") {
            UIApplication.shared.open(url, options: [:])
        }
    }
    
    @IBAction func btnLink2(_ sender: Any) {
        
        let email = "ninjaway@bucketlist.co.jp"
        if let url = URL(string: "mailto:\(email)") {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
        
        /*
         if MFMailComposeViewController.canSendMail() {
         let mail = MFMailComposeViewController()
         mail.mailComposeDelegate = self
         mail.setToRecipients(["ved.ios@yopmail.com"])
         mail.setMessageBody("<p>You're so awesome!</p>", isHTML: true)
         
         present(mail, animated: true)
         } else {
         // show failure alert
         }
         */
    }
    
    @IBAction func btnAsakusaAction(_ sender: Any) {
        
        let objVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AsakusaInfoViewController") as! AsakusaInfoViewController
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    @IBAction func btnHomeMenuAction(_ sender: Any) {
            menuClicked()
    }
    @IBAction func btnHowToPlayMenuAction(_ sender: Any) {
        
        let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        let mainStoryBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let centerVC = mainStoryBoard.instantiateViewController(withIdentifier: "WalkThrough")
        appDel.window!.rootViewController = centerVC
        appDel.window!.makeKeyAndVisible()

    }
    @IBAction func btnContactMenuAction(_ sender: Any) {
        menuClicked()
        imgView.isHidden = false
        blurView.isHidden = false
    }
    //MARK: - Custom Methods
    func menuClicked() {
        //if the hamburger menu is NOT visible, then move the ubeView back to where it used to be
        if !hamburgerMenuIsVisible {
            widthMenu.constant = 250
            //this constant is NEGATIVE because we are moving it 150 points OUTWARD and that means -150
            
            //1
            hamburgerMenuIsVisible = true
        } else {
            //if the hamburger menu IS visible, then move the ubeView back to its original position
            widthMenu.constant = 0
            
            //2
            hamburgerMenuIsVisible = false
        }
        
        UIView.animate(withDuration: 0.05, delay: 0.0, options: .curveEaseIn, animations: {
            self.view.layoutIfNeeded()
        }) { (animationComplete) in
            print("The animation is complete!")
        }
    }
    
    func setUpView() {
        UserDefaults.standard.set(true, forKey: kSessionLoggedIn)
        btnContactUs.titleLabel?.textAlignment = .left
    }
    
    /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
      print("adViewDidReceiveAd")
        
//        addBannerViewToView(self.view)
/*        bannerView.alpha = 0
        UIView.animate(withDuration: 1, animations: {
          bannerView.alpha = 1
        })
*/

    }

    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
        didFailToReceiveAdWithError error: GADRequestError) {
      print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }

    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
      print("adViewWillPresentScreen")
    }

    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
      print("adViewWillDismissScreen")
    }

    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
      print("adViewDidDismissScreen")
    }

    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
      print("adViewWillLeaveApplication")
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
