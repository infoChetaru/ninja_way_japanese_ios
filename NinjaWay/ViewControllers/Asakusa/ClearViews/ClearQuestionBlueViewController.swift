//
//  ClearQuestionBlueViewController.swift
//  NinjaWay
//
//  Created by Apple on 01/03/19.
//  Copyright © 2020 bucketlist. All rights reserved.
//

import UIKit

class ClearQuestionBlueViewController: UIViewController {

    //MARK: - Variables
    var prevVC = UIViewController()

    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        prevVC.dismiss(animated: false, completion: nil)
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Action Methods
    @IBAction func btnNextAction(_ sender: Any) {
        
//        if UserDefaultKeys.getUserPaid() {
//            let objVC = UIStoryboard(name: "Questions", bundle: nil).instantiateViewController(withIdentifier: "QuestionGreenViewController") as! QuestionGreenViewController
//            objVC.prevVC = self
//            self.navigationController?.pushViewController(objVC, animated: false)
//        }
//        else {
            popToMApView()
//        }
    }
    
    //MARK: - Custom Methods
    func popToMApView() {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: MapViewController.self) {
                let vc = controller as? MapViewController
                vc!.showPaymentPopUp = true//pooja added
                self.navigationController!.popToViewController(vc!, animated: false)
                break
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
