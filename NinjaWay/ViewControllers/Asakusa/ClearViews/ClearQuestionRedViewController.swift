//
//  ClearQuestionRedViewController.swift
//  NinjaWay
//
//  Created by Apple on 01/03/19.
//  Copyright © 2020 bucketlist. All rights reserved.
//

import UIKit

class ClearQuestionRedViewController: UIViewController {

    @IBOutlet weak var lblText: UILabel!
    //MARK: - Variables
    var prevVC = UIViewController()
    
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Action Methods
    @IBAction func btnNextAction(_ sender: Any) {
        
//        if UserDefaultKeys.getUserPaid() {
//            let objVC = UIStoryboard(name: "Questions", bundle: nil).instantiateViewController(withIdentifier: "QuestionBlueViewController") as! QuestionBlueViewController
//            objVC.prevVC = self
//            self.navigationController?.pushViewController(objVC, animated: false)
//        }
//        else {
            popToMApView()
//        }
    }
    
  /*  "Trivia of the Kaminarimon,
    “Thunder Gate”
    It, commonly known as Kaminarimon,
     is the main feature of Asakusa and officially called Furaijin-mon with
    a statue of Fujin enshrined on
    the right and that of
    Raijin on the left.
    Fujin and Raijin has enshrined for the first time when the gate
    was relocated to the current location after Kamakura Period.
"*/
    //MARK: - Custom Methods
    func popToMApView() {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: MapViewController.self) {
                let vc = controller as? MapViewController
                vc!.showPaymentPopUp = true//pooja added
                self.navigationController!.popToViewController(vc!, animated: false)
                break
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


