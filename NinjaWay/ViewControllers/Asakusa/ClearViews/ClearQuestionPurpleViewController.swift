//
//  ClearQuestionPurpleViewController.swift
//  NinjaWay
//
//  Created by Apple on 01/03/19.
//  Copyright © 2020 bucketlist. All rights reserved.
//

import UIKit

class ClearQuestionPurpleViewController: UIViewController {
    
    var showClearScreen = false
    
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        prevVC.dismiss(animated: false, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Action Methods
    @IBAction func btnMapAction(_ sender: Any) {
        popToMApView()
    }
    
    @IBAction func btnTapOnThisAction(_ sender: Any) {
//        showClearScreen = true
        popToMApView()
    }
    
    //MARK: - Custom Methods
    func popToMApView() {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: MapViewController.self) {
                let vc = controller as? MapViewController
                vc!.showPaymentPopUp = false
                vc!.showClearScreenPopUp = showClearScreen
                self.navigationController!.popToViewController(controller, animated: false)
                break
            }
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
