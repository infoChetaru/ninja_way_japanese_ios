//
//  MapViewController.swift
//  NinjaWay
//
//  Created by Apple on 20/11/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit
import Stripe
//import NVActivityIndicatorView
import JKNotificationPanel

class MapViewController: UITableViewController, UITextFieldDelegate, UICollectionViewDelegate, UICollectionViewDataSource {

    //MARK: - IBOutlets
    @IBOutlet weak var collectionViewNinja: UICollectionView!
    @IBOutlet weak var answersBlurView: UIView!
    @IBOutlet weak var answersView: UITableView!

    //MARK: - Variables
    let panel = JKNotificationPanel()
    var showPaymentPopUp = false
    var showClearScreenPopUp = false

    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        setDefaults()
        
//        if UserDefaultKeys.getRedAnswerCompleted() && UserDefaultKeys.getBlueAnswerCompleted() && UserDefaultKeys.getPurpleAnswerCompleted() && UserDefaultKeys.getGreenAnswerCompleted() {
//            
//            if showClearScreenPopUp {
//                self.answersBlurView.isHidden = false
//                self.answersView.isHidden = false
//            }
//        }
        
       // btnBlueNinja.flash()
//        btnRedNinja.pulsate()
        
        self.collectionViewNinja.delegate = self
        self.collectionViewNinja.dataSource = self
        
        self.collectionViewNinja.showsHorizontalScrollIndicator = true
        self.collectionViewNinja.showsVerticalScrollIndicator = true
        
        collectionViewNinja.isPagingEnabled = true
        collectionViewNinja.isScrollEnabled = true
        collectionViewNinja.isUserInteractionEnabled = true

     }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tableView.reloadData()
        self.collectionViewNinja.reloadData()
        scrollToBottom()
        
        if UserDefaultKeys.getRedAnswerCompleted() && UserDefaultKeys.getBlueAnswerCompleted() && UserDefaultKeys.getPurpleAnswerCompleted() && UserDefaultKeys.getGreenAnswerCompleted() {

            if showClearScreenPopUp {
                handleAnswerView()
            }
        }

//        if showPaymentPopUp {
//            if UserDefaultKeys.getUserEmail() == "" {
//            }
//            else {
//                self.answersBlurView.isHidden = false
//            }
//        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - IBAction Methods
    func moveToQuesRedVC() {
        let objVC = UIStoryboard(name: "Questions", bundle: nil).instantiateViewController(withIdentifier: "QuestionRedViewController") as! QuestionRedViewController
        self.navigationController?.pushViewController(objVC, animated: false)

    }
    
    func moveToQuesPurpleVC() {
        let objVC = UIStoryboard(name: "Questions", bundle: nil).instantiateViewController(withIdentifier: "QuestionPurpleViewController") as! QuestionPurpleViewController
        self.navigationController?.pushViewController(objVC, animated: false)

    }
    
    func moveToQuesGreenVC() {
        let objVC = UIStoryboard(name: "Questions", bundle: nil).instantiateViewController(withIdentifier: "QuestionGreenViewController") as! QuestionGreenViewController
        self.navigationController?.pushViewController(objVC, animated: false)

    }
    
    func moveToQuesBlueVC() {
        let objVC = UIStoryboard(name: "Questions", bundle: nil).instantiateViewController(withIdentifier: "QuestionBlueViewController") as! QuestionBlueViewController
        self.navigationController?.pushViewController(objVC, animated: false)
    }
    
    @IBAction func btnBackAction() {
        self.answersBlurView.isHidden = true
        self.answersView.isHidden = true
        
        self.tableView.isScrollEnabled = true

    }
    
    func handleAnswerView() {
//        self.answersBlurView.isHidden = true

        
        //        self.answersBlurView.isHidden = answerBlurView
//        self.answersView.isHidden = answerView
//
//        self.tableView.isScrollEnabled = false
        
        self.performSegue(withIdentifier: "toAllClear", sender: nil)
        
    }
    
    //MARK: - Custom Methods
    func scrollToBottom(){
        DispatchQueue.main.async {
            let indexPath = IndexPath(row: 1, section: 0)
            self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: false )
        }
    }

    func showAlert(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let cancelAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    
    func popToHomeView() {
        let objVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        self.navigationController?.pushViewController(objVC, animated: false)
    }
    
    func setDefaults() {
        UserDefaults.standard.set([1,2,3,8], forKey: kRedAnswers)
        UserDefaults.standard.set([16,21], forKey: kBlueAnswers)
        UserDefaults.standard.set([18,23,24,25], forKey: kPurpleAnswers)
        UserDefaults.standard.set([5,10,11,12,13,14,15], forKey: kGreenAnswers)
    }
    
    
    
    // MARK: - UITextField Delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }

    // MARK: - UICollectionViewDelegate and DataSources
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 1
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MapCollectionViewCell", for: indexPath as IndexPath) as! MapCollectionViewCell
        cell.showPaymentPopUp = showPaymentPopUp
        cell.obj = self
        cell.showClearScreenPopUp = showClearScreenPopUp
        cell.ShowAnimate()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: 530, height: 1200)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        
        let totalCellWidth = Int(collectionView.layer.frame.size.width) / 3 * collectionView.numberOfItems(inSection: 0)
        //530 * 1
        let totalSpacingWidth =  (collectionView.numberOfItems(inSection: 0) - 1)
        
        let leftInset = (collectionView.layer.frame.size.width  - CGFloat(totalCellWidth + totalSpacingWidth)) / 2
        let rightInset = leftInset
        
        return UIEdgeInsetsMake(0, leftInset, 0, rightInset)
    }
}


extension UIButton {
    func pulsate() {
        
        let pulse = CASpringAnimation(keyPath: "transform.scale")
        pulse.duration = 0.5
        pulse.fromValue = 0.8
        pulse.toValue = 1.1
        pulse.autoreverses = true
        pulse.repeatCount = 2000
        pulse.initialVelocity = 0.5
        pulse.damping = 5.0
        
        layer.add(pulse, forKey: "pulse")
    }
    
}
