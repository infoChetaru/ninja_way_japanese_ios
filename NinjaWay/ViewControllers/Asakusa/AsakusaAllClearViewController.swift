//
//  AsakusaAllClearViewController.swift
//  NinjaWay
//
//  Created by Apple on 10/04/19.
//  Copyright © 2020 bucketlist. All rights reserved.
//

import UIKit

class AsakusaAllClearViewController: UIViewController {

    var objMapView = MapViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnHomeAction(_ sender: Any) {
        objMapView.btnHomeAction()
        self.dismiss(animated: false, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
