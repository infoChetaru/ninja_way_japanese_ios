//
//  QuestionBlueViewController.swift
//  NinjaWay
//
//  Created by kdstudio on 21/11/18.
//  Copyright © 2020 bucketlist. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView
import JKNotificationPanel

class QuestionBlueViewController: UIViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var imgBubble: UIImageView!
    @IBOutlet weak var answerBtnHeightConstraint: NSLayoutConstraint!
    
    //MARK: - Variables
    let arrActualAnswersIndex = [16,21]
    var arrAnswersUserSelected = [Int]()
    var prevVC = UIViewController()
    let panel = JKNotificationPanel()

    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        prevVC.dismiss(animated: false, completion: nil)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Custom Methods
    func popToMApView() {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: MapViewController.self) {
                let vc = controller as? MapViewController
                vc!.showPaymentPopUp = false
                vc!.showClearScreenPopUp = false
                self.navigationController!.popToViewController(controller, animated: false)
                break
            }
        }
    }
    
    func setUpView() {
        
        setAllAnswers()
        if UserDefaultKeys.getBlueAnswerCompleted() {
            self.imgBubble.isHidden = true
            setOwnAnswers()
            answerBtnHeightConstraint.constant = 0
        }
    }
    
    func setOwnAnswers() {
        
        setAnswers(arrAnswers: arrActualAnswersIndex, image: #imageLiteral(resourceName: "redCross"), enable: true)
        
    }
    
    func setAllAnswers() {
        
        if UserDefaultKeys.getRedAnswerCompleted() {
            setAnswers(arrAnswers: UserDefaults.standard.array(forKey: kRedAnswers) as! [Int], image: #imageLiteral(resourceName: "grayCross"),enable: false)
        }
        
        if UserDefaultKeys.getPurpleAnswerCompleted() {
            setAnswers(arrAnswers: UserDefaults.standard.array(forKey: kPurpleAnswers) as! [Int], image: #imageLiteral(resourceName: "grayCross"),enable: false)
        }
        
        if UserDefaultKeys.getGreenAnswerCompleted() {
            setAnswers(arrAnswers: UserDefaults.standard.array(forKey: kGreenAnswers) as! [Int], image: #imageLiteral(resourceName: "grayCross"), enable: false)
        }
        
    }
    
    func setAnswers(arrAnswers: [Int], image: UIImage, enable: Bool) {
        for value in arrAnswers {
            let btn = self.view.viewWithTag(value) as? UIButton
            btn?.setBackgroundImage(image, for: .normal)
            btn?.isEnabled = enable
        }
    }

    
    //MARK: - Action Methods
    @IBAction func btnBackAction(){
//        self.navigationController?.popViewController(animated: false)
        popToMApView()
    }
    
    @IBAction func btnAnswersAction(sender: UIButton){
        
        if imgBubble.isHidden {
            return
        }
        
        if arrActualAnswersIndex.containsSameElements(as: arrAnswersUserSelected) {
            UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
                self.imgBubble.isHidden = true
                
                    self.callWebServiceToSaveAnswers()
            })
            
        }
        else {
            
            let objVC = UIStoryboard(name: "MissScreens", bundle: nil).instantiateViewController(withIdentifier: "MissQuestionViewController") as! MissQuestionViewController
            objVC.imgMissed = #imageLiteral(resourceName: "MissblueNinjaCloud")
            objVC.modalPresentationStyle = .overCurrentContext
            self.navigationController?.pushViewController(objVC, animated: false)
        }
    }
    
    @IBAction func btnOptionsAction(sender: UIButton){
        
        if imgBubble.isHidden {
            return
        }
        
        sender.isSelected = !sender.isSelected
        
        if sender.isSelected {
            sender.setBackgroundImage(#imageLiteral(resourceName: "redCross"), for: .normal)
            arrAnswersUserSelected.append(sender.tag)
        }
        else {
            sender.setBackgroundImage(nil, for: .normal)
            arrAnswersUserSelected.remove(at: arrAnswersUserSelected.index(of: sender.tag)!)
        }
    }
    
    //MARK: - WebService Methods
    func callWebServiceToSaveAnswers() {
        
        UserDefaultKeys.setBlueAnswerCompleted(blue: true)
        
        let objVC = UIStoryboard(name: "ClearScreens", bundle: nil).instantiateViewController(withIdentifier: "ClearQuestionBlueViewController") as! ClearQuestionBlueViewController
        objVC.prevVC = self
        objVC.modalPresentationStyle = .overCurrentContext
        self.navigationController?.pushViewController(objVC, animated: false)

/*       // NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        let param = [
            "email" : UserDefaultKeys.getUserEmail(),
            "map_name" : "Asakusa",
            "red" : "1",
            "blue" : "1",
            "green" : "0",
            "purple" : "0"
        ]
        
        let strUrl = "http://production.bucketlist.co.uk/ninja/index.php?method=updateAnswer"
        
        WebServiceHandler.postWebService(url: strUrl, param: param, withHeader: false) { (response, errorMsg) in
            
            //NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    UserDefaultKeys.setBlueAnswerCompleted(blue: true)
                    
                    let objVC = UIStoryboard(name: "ClearScreens", bundle: nil).instantiateViewController(withIdentifier: "ClearQuestionBlueViewController") as! ClearQuestionBlueViewController
                    objVC.prevVC = self
                    objVC.modalPresentationStyle = .overCurrentContext
                    self.navigationController?.pushViewController(objVC, animated: false)
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }*/
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
