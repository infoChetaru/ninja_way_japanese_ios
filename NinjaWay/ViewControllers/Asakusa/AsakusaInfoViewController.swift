//
//  AsakusaInfoViewController.swift
//  NinjaWay
//
//  Created by Apple on 05/03/19.
//  Copyright © 2020 bucketlist. All rights reserved.
//

import UIKit
import GoogleMobileAds

class AsakusaInfoViewController: UIViewController {

    @IBOutlet weak var bannerView: GADBannerView!

    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        bannerView.adSize = kGADAdSizeSmartBannerPortrait
        bannerView.adUnitID =  kTestUnitID
        bannerView.rootViewController = self
        bannerView.load(GADRequest())

    }
    
    //MARK: - Action Methods
    @IBAction func btnHomeAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnStartAction(_ sender: Any) {
        let objVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
