//
//  MissQuestionViewController.swift
//  NinjaWay
//
//  Created by Apple on 01/03/19.
//  Copyright © 2020 bucketlist. All rights reserved.
//

import UIKit

class MissQuestionViewController: UIViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var imgViewMissed: UIImageView!
    
    //MARK: - Variables
    var imgMissed: UIImage!

    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        imgViewMissed.image = imgMissed
    }

    //MARK: - IBAction Methods
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
}
