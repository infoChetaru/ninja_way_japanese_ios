//
//  MapViewController.swift
//  NinjaWay
//
//  Created by Apple on 20/11/18.
//  Copyright © 2020 bucketlist. All rights reserved.
//

import UIKit
//import Stripe
import NVActivityIndicatorView
import JKNotificationPanel
import StoreKit
import GoogleMobileAds

let kLiveUnitIDInterstitials = "ca-app-pub-1129577948525988/2744338406"
let kTestUnitIDInterstitials = "ca-app-pub-3940256099942544/4411468910"
let kGreenNinja = 90
let kRedNinja = 10
let kBlueNinja = 30
let kPurpleNinja = 80

//class MapViewController: UIViewController, UITextFieldDelegate, STPPaymentCardTextFieldDelegate, SKPaymentTransactionObserver, SKProductsRequestDelegate {
class MapViewController: UIViewController, UITextFieldDelegate, SKPaymentTransactionObserver, SKProductsRequestDelegate, GADBannerViewDelegate {

    //MARK: - IBOutlets
    @IBOutlet weak var answersBlurView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var paymentView: UIView!
    @IBOutlet weak var paymentTxtView: UIView!
    @IBOutlet weak var btnProceed: UIButton!
    @IBOutlet weak var lblPayment: UILabel!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var btnEmail: UIButton!
    @IBOutlet weak var shoppingView: UIView!
    @IBOutlet weak var omikujiView: UIView!
    @IBOutlet weak var chozushaView: UIView!
    @IBOutlet weak var osaisenView: UIView!
    @IBOutlet weak var smokeView: UIView!
    @IBOutlet weak var mainMapView: UIView!
    @IBOutlet weak var btnPurpleNinja: UIButton!
    @IBOutlet weak var btnGreenNinja: UIButton!
    @IBOutlet weak var imgDestination: UIButton!
    
    @IBOutlet weak var btnRedNinja: UIButton!
    @IBOutlet weak var btnBlueNinja: UIButton!
    @IBOutlet weak var bannerView: GADBannerView!

    //MARK: - Variables
    let panel = JKNotificationPanel()
    var showPaymentPopUp = false
    var showClearScreenPopUp = false
//    let paymentCardTextField = STPPaymentCardTextField()
    var scrollToBottom = true
    var interstitial: GADInterstitial?
    
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        bannerView.adSize = kGADAdSizeSmartBannerPortrait
        bannerView.adUnitID =  kTestUnitID
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        bannerView.delegate = self

        interstitial = createAndLoadInterstitial()

        // Do any additional setup after loading the view.
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        setDefaults()
        
//        if UserDefaultKeys.getRedAnswerCompleted() && UserDefaultKeys.getBlueAnswerCompleted() && UserDefaultKeys.getPurpleAnswerCompleted() && UserDefaultKeys.getGreenAnswerCompleted() {
//            
//            if showClearScreenPopUp {
//                self.answersBlurView.isHidden = false
//                self.answersView.isHidden = false
//            }
//        }
        
       // btnBlueNinja.flash()
//        btnRedNinja.pulsate()
        

//        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap(sender:)))
//        mainMapView.addGestureRecognizer(tap)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imgDestinationTapped(tapGestureRecognizer:)))
        imgDestination.isUserInteractionEnabled = true
        imgDestination.addGestureRecognizer(tapGestureRecognizer)
        
        if UserDefaultKeys.getRedAnswerCompleted() && UserDefaultKeys.getBlueAnswerCompleted() && UserDefaultKeys.getPurpleAnswerCompleted() && UserDefaultKeys.getGreenAnswerCompleted() {
            
            imgDestination.isHidden = true
            
            if showClearScreenPopUp {
                self.handleAnswerView()
                //                self.answersBlurView.isHidden = false
                //                self.answersView.isHidden = false
            }
        }
        
        if !UserDefaultKeys.getRedAnswerCompleted() {
            btnRedNinja.pulsate()
            imgDestination.isHidden = true
        }
        else if UserDefaultKeys.getRedAnswerCompleted() && !UserDefaultKeys.getBlueAnswerCompleted(){
            btnBlueNinja.pulsate()
            imgDestination.isHidden = true
        }
        else if UserDefaultKeys.getRedAnswerCompleted() && UserDefaultKeys.getBlueAnswerCompleted() && !UserDefaultKeys.getGreenAnswerCompleted(){
            btnGreenNinja.pulsate()
            imgDestination.isHidden = true
        }
        else if UserDefaultKeys.getRedAnswerCompleted() && UserDefaultKeys.getBlueAnswerCompleted() && UserDefaultKeys.getGreenAnswerCompleted() && !UserDefaultKeys.getPurpleAnswerCompleted(){
            btnPurpleNinja.pulsate()
            imgDestination.isHidden = true
        }
        
        /*if showPaymentPopUp {
            if UserDefaultKeys.getUserEmail() == "" {
                btnEmailAction(self.btnEmail)
            }
            else {
                txtEmail.text = UserDefaultKeys.getUserEmail()
                self.answersBlurView.isHidden = false
                self.paymentView.isHidden = false
            }
        }*/
        
        if UserDefaultKeys.getRedAnswerCompleted() && UserDefaultKeys.getBlueAnswerCompleted() && UserDefaultKeys.getPurpleAnswerCompleted() && UserDefaultKeys.getGreenAnswerCompleted() {
            //                self.answersBlurView.isHidden = false
            //                self.answersView.isHidden = false
            imgDestination.isHidden = false
            
            if showClearScreenPopUp {
                self.handleAnswerView()
                //                self.answersBlurView.isHidden = false
                //                self.answersView.isHidden = false
            }
        }
        
        //        if !UserDefaultKeys.getRedAnswerCompleted() {
        //            btnRedNinja.pulsate()
        //
        //        }
        //        else if UserDefaultKeys.getRedAnswerCompleted() && !UserDefaultKeys.getBlueAnswerCompleted(){
        //            btnBlueNinja.pulsate()
        //        }
        //        else if UserDefaultKeys.getRedAnswerCompleted() && UserDefaultKeys.getBlueAnswerCompleted() && !UserDefaultKeys.getGreenAnswerCompleted(){
        //            btnGreenNinja.pulsate()
        //        }
        //        else if UserDefaultKeys.getRedAnswerCompleted() && UserDefaultKeys.getBlueAnswerCompleted() && UserDefaultKeys.getGreenAnswerCompleted() && !UserDefaultKeys.getPurpleAnswerCompleted(){
        //            btnPurpleNinja.pulsate()
        //        }
        //
        //        if showPaymentPopUp {
        //            if UserDefaultKeys.getUserEmail() == "" {
        //                btnEmailAction(self.btnEmail)
        //            }
        //            else {
        //                txtEmail.text = UserDefaultKeys.getUserEmail()
        //                self.answersBlurView.isHidden = false
        //                self.paymentView.isHidden = false
        //            }
        //        }
        
        setupMapIcons()
        
//        let bottomOffset = CGPoint(x: 0, y: scrollView.contentSize.height - scrollView.bounds.size.height)
//        scrollView.setContentOffset(bottomOffset, animated: true)

        
     }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        scrollToBottom = true
        self.ShowAnimate()

        if UserDefaultKeys.getRedAnswerCompleted() && UserDefaultKeys.getBlueAnswerCompleted() && UserDefaultKeys.getPurpleAnswerCompleted() && UserDefaultKeys.getGreenAnswerCompleted() {

            if showClearScreenPopUp {
                handleAnswerView()
            }
        }

//        if showPaymentPopUp {
//            if UserDefaultKeys.getUserEmail() == "" {
//            }
//            else {
//                self.answersBlurView.isHidden = false
//            }
//        }
        
        /*if UserDefaultKeys.getUserPaid() {
            btnBlueNinja.setImage(#imageLiteral(resourceName: "ninjaHeadBluePaid"), for: .normal)
            btnGreenNinja.setImage(#imageLiteral(resourceName: "ninjaHeadGreenPaid"), for: .normal)
            btnPurpleNinja.setImage(#imageLiteral(resourceName: "ninjaheadPurpulePaid"), for: .normal)
        }
        else {
            btnBlueNinja.setImage(#imageLiteral(resourceName: "ninjaHeadBlue"), for: .normal)
            btnGreenNinja.setImage(#imageLiteral(resourceName: "ninjaHeadGreen"), for: .normal)
            btnPurpleNinja.setImage(#imageLiteral(resourceName: "ninjaheadPurpule"), for: .normal)
        }*/

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        if scrollToBottom {
            let bottomOffset = CGPoint(x: 0, y: scrollView.contentSize.height - scrollView.bounds.size.height)
            scrollView.setContentOffset(bottomOffset, animated: true)
        }
    }
    
    //MARK:- SKPaymentTransaction Delegates
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        
        print(response.products)
        let count : Int = response.products.count
        if (count>0) {
            
            let validProduct: SKProduct = response.products[0] as SKProduct
            if (validProduct.productIdentifier == product_id as String) {
                print(validProduct.localizedTitle)
                print(validProduct.localizedDescription)
                print(validProduct.price)
                self.buyProduct(product: validProduct)
            } else {
                print(validProduct.productIdentifier)
            }
        } else {
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            print("nothing")
        }
    }
    
    //If an error occurs, the code will go to this function
    func paymentQueue(_ queue: SKPaymentQueue, restoreCompletedTransactionsFailedWithError error: Error) {
        // Show some alert
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction:AnyObject in transactions {
            if let trans:SKPaymentTransaction = transaction as? SKPaymentTransaction{
                
                //self.dismissPurchaseBtn.isEnabled = true
                //self.restorePurchaseBtn.isEnabled = true
                //self.buyNowBtn.isEnabled = true
                switch trans.transactionState {
                case .purchased:
                    print("Product Purchased")
                    btnBlueNinja.setImage(#imageLiteral(resourceName: "ninjaHeadBluePaid"), for: .normal)
                    btnGreenNinja.setImage(#imageLiteral(resourceName: "ninjaHeadGreenPaid"), for: .normal)
                    btnPurpleNinja.setImage(#imageLiteral(resourceName: "ninjaheadPurpulePaid"), for: .normal)
                    //Do unlocking etc stuff here in case of new purchase
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    
                    UserDefaultKeys.setUserPaid(paid: true)
                    break;
                case .failed:
                    print("Purchased Failed");
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)

                    break;
                case .restored:
                    print("Already Purchased")
                    //Do unlocking etc stuff here in case of restor
                    btnBlueNinja.setImage(#imageLiteral(resourceName: "ninjaHeadBluePaid"), for: .normal)
                    btnGreenNinja.setImage(#imageLiteral(resourceName: "ninjaHeadGreenPaid"), for: .normal)
                    btnPurpleNinja.setImage(#imageLiteral(resourceName: "ninjaheadPurpulePaid"), for: .normal)
                    UserDefaultKeys.setUserPaid(paid: true)

                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                default:
                    break;
                }
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            }
            else {
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            }
        }
    }
    
    //MARK: - IBAction Methods
    func moveToClearRedVC() {
        let objVC = UIStoryboard(name: "ClearScreens", bundle: nil).instantiateViewController(withIdentifier: "ClearQuestionRedViewController") as! ClearQuestionRedViewController
        self.navigationController?.pushViewController(objVC, animated: false)
    }
    
    func moveToQuesRedVC() {
        let objVC = UIStoryboard(name: "Questions", bundle: nil).instantiateViewController(withIdentifier: "QuestionRedViewController") as! QuestionRedViewController
        self.navigationController?.pushViewController(objVC, animated: false)
    }
    
    func moveToQuesPurpleVC() {
        let objVC = UIStoryboard(name: "Questions", bundle: nil).instantiateViewController(withIdentifier: "QuestionPurpleViewController") as! QuestionPurpleViewController
        self.navigationController?.pushViewController(objVC, animated: false)
    }
    
    func moveToClearPurpleVC() {
        let objVC = UIStoryboard(name: "ClearScreens", bundle: nil).instantiateViewController(withIdentifier: "ClearQuestionPurpleViewController") as! ClearQuestionPurpleViewController
        self.navigationController?.pushViewController(objVC, animated: false)
    }
    
    func moveToQuesGreenVC() {
        let objVC = UIStoryboard(name: "Questions", bundle: nil).instantiateViewController(withIdentifier: "QuestionGreenViewController") as! QuestionGreenViewController
        self.navigationController?.pushViewController(objVC, animated: false)
    }
    
    func moveToClearGreenVC() {
        let objVC = UIStoryboard(name: "ClearScreens", bundle: nil).instantiateViewController(withIdentifier: "ClearQuestionGreenViewController") as! ClearQuestionGreenViewController
        self.navigationController?.pushViewController(objVC, animated: false)
        
    }
    
    func moveToQuesBlueVC() {
        let objVC = UIStoryboard(name: "Questions", bundle: nil).instantiateViewController(withIdentifier: "QuestionBlueViewController") as! QuestionBlueViewController
        self.navigationController?.pushViewController(objVC, animated: false)
    }
    
    func moveToClearBlueVC() {
        let objVC = UIStoryboard(name: "ClearScreens", bundle: nil).instantiateViewController(withIdentifier: "ClearQuestionBlueViewController") as! ClearQuestionBlueViewController
        self.navigationController?.pushViewController(objVC, animated: false)
    }

    
    @IBAction func btnBackAction() {
        self.answersBlurView.isHidden = true
    }
    
    func handleAnswerView() {
//        self.answersBlurView.isHidden = true

        
        //        self.answersBlurView.isHidden = answerBlurView
//        self.answersView.isHidden = answerView
//
//        self.tableView.isScrollEnabled = false
        
        self.performSegue(withIdentifier: "toAllClear", sender: nil)
        
    }
    
    @objc func imgDestinationTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        //        let tappedImage = tapGestureRecognizer.view as! UIImageView
        
        // Your action
        //        self.answersBlurView.isHidden = false
        //        self.answersView.isHidden = false
        
        self.handleAnswerView()
        
        
    }
    
    @objc func handleTap(sender: UITapGestureRecognizer? = nil) {
        self.shoppingView.isHidden = true
        self.omikujiView.isHidden = true
        self.chozushaView.isHidden = true
        self.smokeView.isHidden = true
        self.osaisenView.isHidden = true
    }
    
    @IBAction func btnCloseAction(sender: UIButton) {
        self.shoppingView.isHidden = true
        self.omikujiView.isHidden = true
        self.chozushaView.isHidden = true
        self.smokeView.isHidden = true
        self.osaisenView.isHidden = true
    }
    
    @IBAction func btnAnnotationAction(sender: UIButton) {
        
        scrollToBottom = false

        self.shoppingView.isHidden = true
        self.omikujiView.isHidden = true
        self.chozushaView.isHidden = true
        self.smokeView.isHidden = true
        self.osaisenView.isHidden = true
        txtEmail.text = UserDefaultKeys.getUserEmail()
        
        switch sender.tag {
        case kRedNinja:
            if UserDefaultKeys.getRedAnswerCompleted() {
                self.moveToClearRedVC()
            }
            else {
                self.moveToQuesRedVC()
            }
        case kPurpleNinja:
                if UserDefaultKeys.getPurpleAnswerCompleted() {
                    if self.interstitial!.isReady {
                        self.interstitial!.present(fromRootViewController: self)
                    } else {
                        print("Ad wasn't ready")
                    }
                    self.moveToClearPurpleVC()
                }
                else if UserDefaultKeys.getGreenAnswerCompleted() {
                    if self.interstitial!.isReady {
                        self.interstitial!.present(fromRootViewController: self)
                    } else {
                        print("Ad wasn't ready")
                    }
                    self.moveToQuesPurpleVC()
                }
                else {
                    self.showAlert(title: "おっとっと！", message: "あなたは緑の忍者を逃したようです!!")
                }
        case kGreenNinja:
//            if UserDefaultKeys.getUserPaid() {
                if UserDefaultKeys.getBlueAnswerCompleted() {
                    if self.interstitial!.isReady {
                        self.interstitial!.present(fromRootViewController: self)
                    } else {
                        print("Ad wasn't ready")
                    }
                    if UserDefaultKeys.getGreenAnswerCompleted() {
                        self.moveToClearGreenVC()
                    }
                    else {
                        self.moveToQuesGreenVC()
                    }
                }
                else {
                    self.showAlert(title: "おっとっと！！", message: "青い忍者を見逃したようです!!")
                }
        case kBlueNinja:
            if UserDefaultKeys.getRedAnswerCompleted() {
                if self.interstitial!.isReady {
                    self.interstitial!.present(fromRootViewController: self)
                } else {
                    print("Ad wasn't ready")
                }
                if UserDefaultKeys.getUserPaid() {
                    if UserDefaultKeys.getBlueAnswerCompleted() {
                        self.moveToClearBlueVC()
                    }
                    else {
                        self.moveToQuesBlueVC()
                    }
                }
                else {
                    
                    let alert = UIAlertController(title: nil, message: "続行するには、以下のオプションを選択してください：", preferredStyle: UIAlertControllerStyle.alert)
                    
                    let cancelAction = UIAlertAction(title: "キャンセル", style: .cancel) { (alertAction) in
                        
                    }
                    
                    SKPaymentQueue.default().add(self)
                    
                    
                    let restoreAction = UIAlertAction(title: "戻す", style: .default) { (alertAction) in
                        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
                        
                        if (SKPaymentQueue.canMakePayments()) {
                            //            SKPaymentQueue.default().add(self)
                            SKPaymentQueue.default().restoreCompletedTransactions()
                        } else {
                            // show error
                        }
                    }
                    
                    let buyAction = UIAlertAction(title: "購入", style: .default) { (alertAction) in
                        self.buyNowAction()
                    }
                    
                    /*
                     */
                    
                    alert.addAction(restoreAction)
                    alert.addAction(buyAction)
                    
                    alert.addAction(cancelAction)
                    
                    self.appDelegate.window?.rootViewController?.present(alert, animated:true, completion: nil)
                    
                    
                    
                    
                    
                    //                if UserDefaultKeys.getUserEmail() != "" {
                    //                    self.answersBlurView.isHidden = false
                    //                    self.paymentView.isHidden = false
                    //                }
                    //                else {
                    //                    self.btnEmailAction(self.btnEmail)
                    //                }
                    
                }
                
                
            }
            else {
                self.showAlert(title: "おっとっと！！", message: "赤い忍者を見逃したようです!!")
            }
        
            
            /*if UserDefaultKeys.getUserPaid() {
                if UserDefaultKeys.getRedAnswerCompleted() {
                    if UserDefaultKeys.getBlueAnswerCompleted() {
                        self.moveToClearBlueVC()
                    }
                    else {
                        self.moveToQuesBlueVC()
                    }
                }
                else {
                    self.showAlert(title: "Oops!", message: "It seems you missed out the Red Ninja!!")
                }
            }
            else {
                
                let alert = UIAlertController(title: nil, message: "Please select an option below to continue:", preferredStyle: UIAlertControllerStyle.alert)
                
                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (alertAction) in
                    
                }
                
                SKPaymentQueue.default().add(self)

                
                let restoreAction = UIAlertAction(title: "Restore", style: .default) { (alertAction) in
                    NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)

                    if (SKPaymentQueue.canMakePayments()) {
                        //            SKPaymentQueue.default().add(self)
                        SKPaymentQueue.default().restoreCompletedTransactions()
                    } else {
                        // show error
                    }
                }
                
                let buyAction = UIAlertAction(title: "Buy", style: .default) { (alertAction) in
                    self.buyNowAction()
                }
                
                /*
                 */
                
                alert.addAction(restoreAction)
                alert.addAction(buyAction)

                alert.addAction(cancelAction)
                
                self.appDelegate.window?.rootViewController?.present(alert, animated:true, completion: nil)
                
                

                
                
//                if UserDefaultKeys.getUserEmail() != "" {
//                    self.answersBlurView.isHidden = false
//                    self.paymentView.isHidden = false
//                }
//                else {
//                    self.btnEmailAction(self.btnEmail)
//                }
                
            }*/
            
        case 20:
            self.shoppingView.isHidden = false
            self.omikujiView.isHidden = true
            self.chozushaView.isHidden = true
            self.smokeView.isHidden = true
            self.osaisenView.isHidden = true
            
        case 40:
            self.shoppingView.isHidden = true
            self.omikujiView.isHidden = false
            self.chozushaView.isHidden = true
            self.smokeView.isHidden = true
            self.osaisenView.isHidden = true
            
        case 60:
            self.shoppingView.isHidden = true
            self.omikujiView.isHidden = true
            self.chozushaView.isHidden = true
            self.smokeView.isHidden = false
            self.osaisenView.isHidden = true
            
        case 70:
            self.shoppingView.isHidden = true
            self.omikujiView.isHidden = true
            self.chozushaView.isHidden = true
            self.smokeView.isHidden = true
            self.osaisenView.isHidden = false
            
        case 50:
            self.shoppingView.isHidden = true
            self.omikujiView.isHidden = true
            self.chozushaView.isHidden = false
            self.smokeView.isHidden = true
            self.osaisenView.isHidden = true
            
        default:
            print("")
        }
    }
    
    @IBAction func btnHomeAction() {
        self.popToHomeView()
    }
    
    //New
    @IBAction func btnResetAction(_ sender: Any) {
        
        let defaults = UserDefaults.standard
        defaults.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
        
        self.sendToWalkthrough()
        
    }

    @IBAction func btnEmailAction(_ sender: Any) {
        
        let alert = UIAlertController(title: "Login", message: "Please enter/change your email address.", preferredStyle: UIAlertControllerStyle.alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (alertAction) in
        }
        let action = UIAlertAction(title: "Submit", style: .destructive) { (alertAction) in
            let textField = alert.textFields![0] as UITextField
            self.btnEmail.setTitle("  " + textField.text! + "  ", for: .normal)
            self.view.endEditing(true)
            self.callWebServiceToGetTransaction(email: textField.text!)
        }
        /*
        */
        
        alert.addTextField { (textField) in
            textField.placeholder = "Enter your Email Address"
            textField.text = UserDefaultKeys.getUserEmail()
        }
        alert.addAction(action)
        alert.addAction(cancelAction)
        
        self.appDelegate.window?.rootViewController?.present(alert, animated:true, completion: nil)
        
        //}
    }
    
    @IBAction func btnProceedAction() {
        
        /*if btnProceed.titleLabel?.text == "   Proceed    " {
            
            if (txtEmail.text?.isEmpty)! {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter email address.")
            }
            else if (txtEmail.text?.isValidEmail())! {
                btnProceed.isEnabled = false
                btnProceed.alpha = 0.6
                
                paymentCardTextField.delegate = self
                
                // Add payment card text field to view
                paymentCardTextField.frame = CGRect.init(x: 0, y: 0, width: paymentTxtView.frame.size.width, height: paymentTxtView.frame.size.height)
                paymentTxtView.addSubview(paymentCardTextField)
                lblPayment.isHidden = true
                txtEmail.isHidden = true
                btnProceed.setTitle("   Confirm   ", for: .normal)
            }
            else {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter valid email address.")
            }
        }
        else {
            if (txtEmail.text?.isEmpty)! {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter email address.")
            }
            else if (txtEmail.text?.isValidEmail())! {
                stripePayment()
            }
            else {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter valid email address.")
            }
        }*/
    }
    
    @IBAction func btnClosePayment() {
        answersBlurView.isHidden = true
        paymentView.isHidden = true
    }
    
    //MARK: - Custom Methods
    func buyProduct(product: SKProduct)
    {
        let payment = SKPayment(product: product)
        SKPaymentQueue.default().add(payment)
        SKPaymentQueue.default().add(self)
    }
    
    func buyNowAction() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        if (SKPaymentQueue.canMakePayments()) {
            let productID:NSSet = NSSet(array: [product_id as NSString]);
            let productsRequest:SKProductsRequest = SKProductsRequest(productIdentifiers: productID as! Set<String>);
            productsRequest.delegate = self;
            productsRequest.start();
            print("Fetching Products");
        } else {
            print("can't make purchases");
        }
    }
    
//    func getUserPaid() -> Bool{
//        if btnBlueNinja.imageView?.image == #imageLiteral(resourceName: "ninjaHeadBluePaid") {
//            return true
//        }
//        else {
//            return false
//        }
//    }
    
    func showAlert(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let cancelAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func sendToWalkthrough() {
        
        let objVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WalkThrough1ViewController") 
        self.navigationController?.pushViewController(objVC, animated: false)

    }
    
    func popToHomeView() {

        let objVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        self.navigationController?.pushViewController(objVC, animated: false)
    }
    
    func setDefaults() {
        UserDefaults.standard.set([1,2,3,8], forKey: kRedAnswers)
        UserDefaults.standard.set([16,21], forKey: kBlueAnswers)
        UserDefaults.standard.set([18,23,24,25], forKey: kPurpleAnswers)
        UserDefaults.standard.set([5,10,11,12,13,14,15], forKey: kGreenAnswers)
    }
    
    /*func paymentCardTextFieldDidChange(_ textField: STPPaymentCardTextField) {
        
        if textField.isValid {
            btnProceed.isEnabled = true
            btnProceed.alpha = 1.0
        }
        else {
            btnProceed.isEnabled = false
            btnProceed.alpha = 0.6
        }
    }
    
    func stripePayment() {
        self.view.endEditing(true)
        //NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let cardParams = STPCardParams()
        cardParams.number = paymentCardTextField.cardNumber
        cardParams.expMonth = paymentCardTextField.expirationMonth
        cardParams.expYear = paymentCardTextField.expirationYear
        cardParams.cvc = paymentCardTextField.cvc
        
        STPAPIClient.shared().createToken(withCard: cardParams) { (token, error) in
            guard let token = token, error == nil else {
                // Present error to user...
                print(error?.localizedDescription as Any)
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: error?.localizedDescription)
                //NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                return
            }
            
            self.callWebServicePayment(token: token.tokenId)
        }
    }*/
    
    func ShowAnimate(){
        
        if !UserDefaultKeys.getRedAnswerCompleted() {
            btnRedNinja.pulsate()
        }
        else if UserDefaultKeys.getRedAnswerCompleted() && !UserDefaultKeys.getBlueAnswerCompleted(){
            btnBlueNinja.pulsate()
        }
        else if UserDefaultKeys.getRedAnswerCompleted() && UserDefaultKeys.getBlueAnswerCompleted() && !UserDefaultKeys.getGreenAnswerCompleted(){
            btnGreenNinja.pulsate()
        }
        else if UserDefaultKeys.getRedAnswerCompleted() && UserDefaultKeys.getBlueAnswerCompleted() && UserDefaultKeys.getGreenAnswerCompleted() && !UserDefaultKeys.getPurpleAnswerCompleted(){
            btnPurpleNinja.pulsate()
        }
        
//        if showPaymentPopUp {
//            if UserDefaultKeys.getUserEmail() == "" {
//                btnEmailAction(self.btnEmail)
//            }
//            else {
//                txtEmail.text = UserDefaultKeys.getUserEmail()
//                self.answersBlurView.isHidden = false
//                self.paymentView.isHidden = false
//            }
//        }
        
        if UserDefaultKeys.getRedAnswerCompleted() && UserDefaultKeys.getBlueAnswerCompleted() && UserDefaultKeys.getPurpleAnswerCompleted() && UserDefaultKeys.getGreenAnswerCompleted() {
            
            imgDestination.isHidden = false
            
            if showClearScreenPopUp {
                self.handleAnswerView()
            }
        }
        else {
            imgDestination.isHidden = true
        }
    }
    
    func setupMapIcons() {
        
        return
//        let email = UserDefaultKeys.getUserEmail()
        let paid = UserDefaultKeys.getUserPaid()
        
//        if (email != "") {
//            let str = "  " + email + "  "
//            btnEmail.setTitle(str, for: .normal)
//        }
//        else {
//            btnEmail.setTitle("  Login  ", for: .normal)
//        }
        
        if (paid != nil) {
            let paid = paid
            if paid {
                btnBlueNinja.setBackgroundImage(#imageLiteral(resourceName: "ninjaHeadBluePaid"), for: .normal)
                btnGreenNinja.setBackgroundImage(#imageLiteral(resourceName: "ninjaHeadGreenPaid"), for: .normal)
                btnPurpleNinja.setBackgroundImage(#imageLiteral(resourceName: "ninjaheadPurpulePaid"), for: .normal)
            }
            else {
                btnBlueNinja.setBackgroundImage(#imageLiteral(resourceName: "ninjaHeadBlue"), for: .normal)
                btnGreenNinja.setBackgroundImage(#imageLiteral(resourceName: "ninjaHeadGreen"), for: .normal)
                btnPurpleNinja.setBackgroundImage(#imageLiteral(resourceName: "ninjaheadPurpule"), for: .normal)
            }
        }
        else {
            btnBlueNinja.setBackgroundImage(#imageLiteral(resourceName: "ninjaHeadBlue"), for: .normal)
            btnGreenNinja.setBackgroundImage(#imageLiteral(resourceName: "ninjaHeadGreen"), for: .normal)
            btnPurpleNinja.setBackgroundImage(#imageLiteral(resourceName: "ninjaheadPurpule"), for: .normal)
        }
        
    }
    
    
    // MARK: - UITextField Delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    //MARK: - GADInterstitial Ads
    func createAndLoadInterstitial() -> GADInterstitial {
        interstitial = GADInterstitial(adUnitID: kTestUnitIDInterstitials)
        interstitial!.delegate = self
        interstitial!.load(GADRequest())
        return interstitial!
    }
    
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        interstitial = createAndLoadInterstitial()
    }
    
    // MARK: - GADBannerView Delegate
    
    // Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("adViewDidReceiveAd")
        
        //        addBannerViewToView(self.view)
        /*        bannerView.alpha = 0
         UIView.animate(withDuration: 1, animations: {
         bannerView.alpha = 1
         })
         */
        
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }

    
    //MARK: - WebService Methods
    func callWebServicePayment(token: String) {
        
        let param = ["stripeToken" : token,
                     "email": txtEmail.text!,
                     "amount": "100",
                     "currency": "JPY",
                     "description": "Ninja Unlocked!"] as [String : Any]
        
        let strUrl = "http://production.bucketlist.co.uk/ninja/index.php?method=initiatePayment"
        
        WebServiceHandler.postWebService(url: strUrl, param: param, withHeader: false) { (response, errorMsg) in
            
            //NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    
                    UserDefaultKeys.setUserDetails(email: self.txtEmail.text!, paid: true)
                    
                    let alertController = UIAlertController(title: "Congratulations!", message: "You successfully unlocked all the Ninja's.", preferredStyle: UIAlertControllerStyle.alert)
                    
                    alertController.addAction(UIAlertAction(title: "Play", style: .destructive, handler: { action in
                        //run your function here
                        self.answersBlurView.isHidden = true
                        self.paymentView.isHidden = true
                        self.setupMapIcons()
                    }))
                    
                    self.appDelegate.window?.rootViewController?.present(alertController, animated: true, completion: nil)
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    func callWebServiceToGetTransaction(email: String) {
        
        //NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        let param = ["email" : email]
        
        let strUrl = "http://production.bucketlist.co.uk/ninja/index.php?method=getTransaction"
        
        WebServiceHandler.postWebService(url: strUrl, param: param, withHeader: false) { (response, errorMsg) in
            
            // NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    
                    UserDefaultKeys.setUserAnswrs(red: response!["result"]["answers"][0]["red"].boolValue, blue: response!["result"]["answers"][0]["blue"].boolValue, purple: response!["result"]["answers"][0]["purple"].boolValue, green: response!["result"]["answers"][0]["green"].boolValue)
                    
                    if !UserDefaultKeys.getRedAnswerCompleted() && UserDefaultKeys.getUserEmail() == "" {
                        self.callWebServiceToSaveAnswers(email: email)
                    }
                    
                    if response!["result"]["transactions"].arrayValue.count > 0 {
                        UserDefaultKeys.setUserDetails(email: email, paid: true)
                    }
                    else {
                        UserDefaultKeys.setUserDetails(email: email, paid: false)
                    }
                    
                    
                    self.answersBlurView.isHidden = true
                    self.paymentView.isHidden = true
                    
                    if UserDefaultKeys.getRedAnswerCompleted() && UserDefaultKeys.getBlueAnswerCompleted() && UserDefaultKeys.getPurpleAnswerCompleted() && UserDefaultKeys.getGreenAnswerCompleted() {
                        //                        self.answersBlurView.isHidden = false
                        //                        self.answersView.isHidden = false
                        self.handleAnswerView()
                        
                    }
                    
                    
                    self.setupMapIcons()
                    
                    
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    func callWebServiceToSaveAnswers(email: String) {
        
        //NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        let param = [
            "email" : email,
            "map_name" : "Asakusa",
            "red" : "1",
            "blue" : "0",
            "green" : "0",
            "purple" : "0"
        ]
        
        let strUrl = "http://production.bucketlist.co.uk/ninja/index.php?method=postAnswer"
        
        WebServiceHandler.postWebService(url: strUrl, param: param, withHeader: false) { (response, errorMsg) in
            
            //NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    UserDefaultKeys.setRedAnswerCompleted(red: true)
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toAllClear" {
            let vc = segue.destination as! AsakusaAllClearViewController
            vc.objMapView = self
        }
    }
}


extension UIButton {
    func pulsate() {
        
        let pulse = CASpringAnimation(keyPath: "transform.scale")
        pulse.duration = 0.5
        pulse.fromValue = 0.8
        pulse.toValue = 1.1
        pulse.autoreverses = true
        pulse.repeatCount = 2000
        pulse.initialVelocity = 0.5
        pulse.damping = 5.0
        
        layer.add(pulse, forKey: "pulse")
    }
    
}

extension MapViewController: GADInterstitialDelegate {
    func interstitialDidReceiveAd(_ ad: GADInterstitial) {
        print("Interstitial loaded successfully")
        
//        ad.present(fromRootViewController: self)
    }

    func interstitialDidFail(toPresentScreen ad: GADInterstitial) {
        print("Fail to receive interstitial")
    }
}
