//
//  PopupViewViewController.swift
//  NinjaWay
//
//  Created by Apple on 01/03/19.
//  Copyright © 2020 bucketlist. All rights reserved.
//

import UIKit

class InfoPopupViewViewController: UIViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var lblheader: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    //MARK: - Variables
    var strTitle = ""
    var strDescription = ""
    
    //MARK: - LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        lblheader.text = strTitle
        lblDescription.text = strDescription
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK: - IBAction Methods
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    

}
