//
//  PopupViewViewController.swift
//  NinjaWay
//
//  Created by Apple on 01/03/19.
//  Copyright © 2019 chetaru. All rights reserved.
//

import UIKit

class InfoPopupViewViewController: UIViewController {

    @IBOutlet weak var lblheader: UILabel!
    
    @IBOutlet weak var lblDescription: UILabel!
    
    var strTitle = ""
    var strDescription = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        lblheader.text = strTitle
        lblDescription.text = strDescription
        
        // Do any additional setup after loading the view.
    }

    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
//    self.dismiss(animated: false, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
