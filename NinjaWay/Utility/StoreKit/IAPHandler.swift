//
//  IAPHandler.swift
//  NinjaWay
//
//  Created by Apple on 27/06/19.
//  Copyright © 2020 bucketlist. All rights reserved.
//

import UIKit
import StoreKit

let product_id: NSString = "com.bucketlist.NinjaWayJapanese.premiumNinjas" //"com.bucketlist.NinjaWay.premiumNinjas" // <!-- Change it to your inapp id

class IAPHandler: NSObject, SKProductsRequestDelegate, SKPaymentTransactionObserver {

    func initiatePayment() {
        SKPaymentQueue.default().add(self)
    }

    func buyNowAction() {
        
        if (SKPaymentQueue.canMakePayments()) {
            let productID:NSSet = NSSet(array: [product_id as NSString]);
            let productsRequest:SKProductsRequest = SKProductsRequest(productIdentifiers: productID as! Set<String>);
            productsRequest.delegate = self;
            productsRequest.start();
            print("Fetching Products");
        } else {
            print("can't make purchases");
        }
    }
    
    func restoreAction() {
        
        if (SKPaymentQueue.canMakePayments()) {
            //            SKPaymentQueue.default().add(self)
            SKPaymentQueue.default().restoreCompletedTransactions()
        } else {
            // show error
        }
    }
    
    func buyProduct(product: SKProduct)
    {
        let payment = SKPayment(product: product)
        SKPaymentQueue.default().add(payment)
        SKPaymentQueue.default().add(self)
    }
    
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        
        print(response.products)
        let count : Int = response.products.count
        if (count>0) {
            
            let validProduct: SKProduct = response.products[0] as SKProduct
            if (validProduct.productIdentifier == product_id as String) {
                print(validProduct.localizedTitle)
                print(validProduct.localizedDescription)
                print(validProduct.price)
                self.buyProduct(product: validProduct)
            } else {
                print(validProduct.productIdentifier)
            }
        } else {
            print("nothing")
        }
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction:AnyObject in transactions {
            if let trans:SKPaymentTransaction = transaction as? SKPaymentTransaction{
                
                //self.dismissPurchaseBtn.isEnabled = true
                //self.restorePurchaseBtn.isEnabled = true
                //self.buyNowBtn.isEnabled = true
                
                switch trans.transactionState {
                case .purchased:
                    print("Product Purchased")
                    //Do unlocking etc stuff here in case of new purchase
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    break;
                case .failed:
                    print("Purchased Failed");
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    break;
                case .restored:
                    print("Already Purchased")
                    //Do unlocking etc stuff here in case of restor
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Restore_Success"), object: nil)
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                default:
                    break;
                }
            }
        }
    }
    
    //If an error occurs, the code will go to this function
    func paymentQueue(_ queue: SKPaymentQueue, restoreCompletedTransactionsFailedWithError error: Error) {
        // Show some alert
    }
}


