//
//  WebServiceHandler.swift
//  Tribe365
//
//  Created by kdstudio on 29/05/18.
//  Copyright © 2020 bucketlist. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class WebServiceHandler: NSObject {
    
    class func postWebService(url:String, param:[String:Any], withHeader: Bool, completionHandler: @escaping (JSON?, String?) -> Void) ->  Void {
        
        let defaults = UserDefaults.standard
        
        let token = withHeader ? ("Bearer " + (defaults.value(forKey: "token") as! String)) : ""
        
        let headers = withHeader ? ["Authorization":token,"Accept":"application/json","Content-Type":"application/json"] : nil
        
        Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding.default, headers: withHeader ? headers : nil).responseJSON { response in
            
            switch response.result {
            case .success(let value):
                print(response)
                let json = JSON(value)
                completionHandler(json,json["message"].stringValue)
                break
            case .failure(let error):
                completionHandler(nil,error.localizedDescription)
            }
        }

    }
    
    class func getWebService(url:String, param:[String:Any]?, withHeader: Bool, completionHandler: @escaping (JSON?, String?) -> Void) ->  Void {
        
        let defaults = UserDefaults.standard
        
        let token = withHeader ? ("Bearer " + (defaults.value(forKey: "token") as! String)) : ""
        
        let headers = withHeader ? ["Authorization":token,"Accept":"application/json","Content-Type":"application/json"] : nil
        
        Alamofire.request(url, method: .get, parameters: param, encoding: JSONEncoding.default, headers: withHeader ? headers : nil).responseJSON { response in
            
            switch response.result {
            case .success(let value):
                print(response)
                let json = JSON(value)
                completionHandler(json,json["message"].stringValue)
                break
            case .failure(let error):
                completionHandler(nil,error.localizedDescription)
            }
        }
        
    }
    
    class func postMultiPartReq(fileParamName: String,url:String, docUrl: URL? = nil, imageData: Data? = nil, parameters:[String:Any]? = nil, withHeader: Bool, completionHandler: @escaping (JSON?, String?) -> Void) ->  Void {
        
        let defaults = UserDefaults.standard
        let token = withHeader ? ("Bearer " + (defaults.value(forKey: "token") as! String)) : ""
        let headers = withHeader ? ["Authorization":token,
                                    "Accept":"application/json",
                                    "Content-Type":"multipart/form-data"] : nil

        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            if parameters != nil {
                for (key, value) in parameters! {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }
            
            if imageData != nil{
                if let data = imageData{
                    
                    multipartFormData.append(data, withName: fileParamName, fileName: "\(arc4random()).png", mimeType: "image/png")
                }
                
            }
            else
            {
                if docUrl != nil {
                    
                    do {
                        let fileData = try Data(contentsOf: docUrl!)
                        let data : Data = fileData
                        let compressedData = fileData//fileData.compress(withAlgorithm: .zlib)
                        let fileName = docUrl?.lastPathComponent
                        let mimeType: String?
                        
                        switch docUrl!.pathExtension {
                        case "pdf":
                            mimeType = "application/pdf"
                            break
                            
                        case "txt":
                            mimeType = "text/plain"
                            break
                            
                        case "doc":
                            mimeType = "application/msword"
                            break
                            
                        case "xml":
                            mimeType = "application/xml"
                            break
                            
                        case "ppt":
                            mimeType = "application/powerpoint"
                            break
                            
                        default:
                            mimeType = "image/png"
                            break
                            
                        }
                        
                        multipartFormData.append(compressedData, withName: fileParamName, fileName: fileName!, mimeType: mimeType!)
                    }catch let error as NSError {
                        print(error)
                    }
                    
                    
                }
            }
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    print(response)
                    
                    switch response.result {
                    case .success(let value):
                        let json = JSON(value)
                        print("JSON: \(json)")
                        completionHandler(json, nil)
                    case .failure(let error):
                        print(error)
                        completionHandler(nil,error.localizedDescription)
                    }
                    
//                    if let err = response.error{
//                        completionHandler(nil,err.localizedDescription)
//                        return
//                    }
//                    else {
//                        print("Succesfully uploaded")
//                        let json = JSON(response.result.value)
//                        completionHandler(json, nil)
//                    }
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                completionHandler(nil,error.localizedDescription)
            }
        }
    }
}
