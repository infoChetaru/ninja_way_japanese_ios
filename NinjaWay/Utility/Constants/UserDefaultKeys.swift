//
//  UserDefaultKeys.swift
//  NinjaWay
//
//  Created by kdstudio on 21/11/18.
//  Copyright © 2020 bucketlist. All rights reserved.
//

import UIKit

let kRedQuestionCompleted = "FisrtCompleted"
let kBlueQuestionCompleted = "SecondCompleted"
let kPurpleQuestionCompleted = "ThirdCompleted"
let kGreenQuestionCompleted = "FourthCompleted"

let kRedAnswers = "FisrtAnswers"
let kBlueAnswers = "SecondAnswers"
let kPurpleAnswers = "ThirdAnswers"
let kGreenAnswers = "FourthAnswers"

let kSessionLoggedIn = "Session"

let kUserEmail = "Email"
let kUserPaid = "Paid"

class UserDefaultKeys: NSObject {

    class func getUserEmail() -> String{
//        return "" //pooja added
        return (UserDefaults.standard.value(forKey: kUserEmail) as? String ?? "")
    }
    
    class func getUserPaid() -> Bool {
        return true //pooja added
//        return (UserDefaults.standard.value(forKey: kUserPaid) as? Bool ?? false)
    }
    
    class func setUserPaid(paid: Bool) {
        let defaults = UserDefaults.standard
        defaults.set(paid, forKey: kUserPaid)
    }
    
    class func setUserDetails(email: String, paid: Bool) {
        let defaults = UserDefaults.standard
        defaults.set(email, forKey: kUserEmail)
        defaults.set(paid, forKey: kUserPaid)
    }
    
    class func setUserAnswrs(red: Bool, blue: Bool, purple: Bool, green: Bool) {
        UserDefaults.standard.set(red, forKey: kRedQuestionCompleted)
        UserDefaults.standard.set(blue, forKey: kBlueQuestionCompleted)
        UserDefaults.standard.set(green, forKey: kGreenQuestionCompleted)
        UserDefaults.standard.set(purple, forKey: kPurpleQuestionCompleted)
    }
    
    class func setRedAnswerCompleted(red: Bool) {
        UserDefaults.standard.set(red, forKey: kRedQuestionCompleted)
    }
    
    class func setBlueAnswerCompleted(blue: Bool) {
        UserDefaults.standard.set(blue, forKey: kBlueQuestionCompleted)
    }
    
    class func setPurpleAnswerCompleted(purple: Bool) {
        UserDefaults.standard.set(purple, forKey: kPurpleQuestionCompleted)
    }
    
    class func setGreenAnswerCompleted(green: Bool) {
        UserDefaults.standard.set(green, forKey: kGreenQuestionCompleted)
    }
    
    class func getRedAnswerCompleted() -> Bool {
        return (UserDefaults.standard.value(forKey: kRedQuestionCompleted) as? Bool ?? false)
    }
    
    class func getBlueAnswerCompleted() -> Bool {
        return (UserDefaults.standard.value(forKey: kBlueQuestionCompleted) as? Bool ?? false)
    }
    
    class func getPurpleAnswerCompleted() -> Bool {
        return (UserDefaults.standard.value(forKey: kPurpleQuestionCompleted) as? Bool ?? false)
    }
    
    class func getGreenAnswerCompleted() -> Bool {
        return (UserDefaults.standard.value(forKey: kGreenQuestionCompleted) as? Bool ?? false)
    }
}
